import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType
});

Router.map(function() {
  this.mount('pt-login');
  // this.mount('pt-login', { as: 'login' });
});

export default Router;
